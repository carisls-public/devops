# Devops Scripts

Scripts to be used for building and deployment.

## Build Image

Setup for building and deploying of a `Dockerfile`. If a `Dockerfile` is not in
the root of the repository, a variable DOCKERFILE_PATH needs to be provided
with a path relative to the repository root.

By default, image is pushed to `us.gcr.io/caris-ops/${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}`. If `CI_PROJECT_NAME` is
different from the desired name, it is good to set `APP` variable
and that one will take precedence.

```yaml
include:
  - project: carisls/web/devops
    file: /templates/build-image.yml

build-image:
  stage: build-image
  extends: .build-image
  variables:
    DOCKERFILE_PATH: some-folder/Dockerfile.prod # This is optional
  only:
    - develop
```

## Deploy to Kubernetes

To add develop deployment with standard settings, you need to add this:

```yaml
include:
  - project: carisls/web/devops
    file: /templates/deploy/develop.yml

build-image:
  extends: .deploy_develop
  variables:
    APP: my-app # This is required (can be a global variable)
  only:
    - develop # This is optional (default setting is develop)
```

Similar to this, you can add deployments to all other environments
by changing `extends` and file imported:

**Staging**:
- file: `/templates/deploy/staging.yml`
- extends: `.deploy_staging`

**Production**:
- file: `/templates/deploy/production.yml`
- extends: `.deploy_prod`
